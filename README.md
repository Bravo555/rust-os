Getting started:

```
$ rustup override set nightly
$ cargo install bootimage
$ cargo install cargo-xbuild
$ rustup component add llvm-tools-preview
```
